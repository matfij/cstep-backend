// Automatically generated file by swagger_to. DO NOT EDIT OR APPEND ANYTHING!

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { RequestOptions } from '@angular/http';

export interface User {
    id?: number;

    login: string;

    password: string;

    email: string;

    first_name: string;

    last_name: string;

    index: string;

    token?: string;

    user_role: number;

    group_id?: string;
}

export interface SimulationParameters {
    id?: number;

    simulation_parameters_id: string;

    task_id: number;

    user_id: number;

    gain_k: number;

    gain_p: number;

    gain_i: number;
}

export interface Task {
    id?: number;

    group_id: string;

    start_date: string;

    end_date: string;
}

export interface LoginParams {
    login: string;

    password: string;
}

export interface ResultMessage {
    message?: string;

    code?: number;
}

@Injectable()
export class RemoteCaller {
    public url_prefix: string;
    public on_error: (error: HttpErrorResponse) => Observable<HttpErrorResponse> | null;

    constructor(private http: Http) {
        this.url_prefix = "";
        this.on_error = null;
    }

    public set_url_prefix(url_prefix: string) {
        this.url_prefix = url_prefix;
    }

    public set_on_error(on_error: (error: HttpErrorResponse) => Observable<HttpErrorResponse> | null) {
        this.on_error = on_error;
    }

    // Sends a request to the endpoint: /users/add post
    public createUser(user: User): Observable<User | HttpErrorResponse> {
        const url = this.url_prefix + "users/add";

        let observable = this.http.request(url, 
            new RequestOptions({method: "post", body: JSON.stringify(user)}));
        let typed_observable = observable.map(res => (res.json() as User));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /users/get/{id} get
    public getUser(id: number): Observable<User | HttpErrorResponse> {
        let url = this.url_prefix;
        url += encodeURIComponent("users/get/");
        url += encodeURIComponent(id.toString());
        let observable = this.http.get(url);
        let typed_observable = observable.map(res => (res.json() as User));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /users/getAll get
    public getAllUsers(): Observable<Array<User> | HttpErrorResponse> {
        const url = this.url_prefix + "users/getAll";

        let observable = this.http.get(url);
        let typed_observable = observable.map(res => (res.json() as Array<User>));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /users/update put
    public updateUser(user: User): Observable<User | HttpErrorResponse> {
        const url = this.url_prefix + "users/update";

        let observable = this.http.request(url, 
            new RequestOptions({method: "put", body: JSON.stringify(user)}));
        let typed_observable = observable.map(res => (res.json() as User));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /users/delete/{id} delete
    public deleteUser(id: number): Observable<ResultMessage | HttpErrorResponse> {
        let url = this.url_prefix;
        url += encodeURIComponent("users/delete/");
        url += encodeURIComponent(id.toString());
        let observable = this.http.delete(url);
        let typed_observable = observable.map(res => (res.json() as ResultMessage));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /users/register post
    public registerUser(user: User): Observable<User | HttpErrorResponse> {
        const url = this.url_prefix + "users/register";

        let observable = this.http.request(url, 
            new RequestOptions({method: "post", body: JSON.stringify(user)}));
        let typed_observable = observable.map(res => (res.json() as User));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /users/login post
    public login(loginParams: LoginParams): Observable<User | HttpErrorResponse> {
        const url = this.url_prefix + "users/login";

        let observable = this.http.request(url, 
            new RequestOptions({method: "post", body: JSON.stringify(loginParams)}));
        let typed_observable = observable.map(res => (res.json() as User));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /simulationParameters/add post
    public createSimulationParameters(
            simulationParameters: SimulationParameters): Observable<SimulationParameters | HttpErrorResponse> {
        const url = this.url_prefix + "simulationParameters/add";

        let observable = this.http.request(url, 
            new RequestOptions({method: "post", body: JSON.stringify(simulationParameters)}));
        let typed_observable = observable.map(res => (res.json() as SimulationParameters));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /simulationParameters/get/{id} get
    public getSimulationParameters(id: number): Observable<SimulationParameters | HttpErrorResponse> {
        let url = this.url_prefix;
        url += encodeURIComponent("simulationParameters/get/");
        url += encodeURIComponent(id.toString());
        let observable = this.http.get(url);
        let typed_observable = observable.map(res => (res.json() as SimulationParameters));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /simulationParameters/getAll get
    public getAllSimulationParameters(): Observable<SimulationParameters | HttpErrorResponse> {
        const url = this.url_prefix + "simulationParameters/getAll";

        let observable = this.http.get(url);
        let typed_observable = observable.map(res => (res.json() as SimulationParameters));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /simulationParameters/update put
    public updateSimulationParameters(
            simulationParameters: SimulationParameters): Observable<User | HttpErrorResponse> {
        const url = this.url_prefix + "simulationParameters/update";

        let observable = this.http.request(url, 
            new RequestOptions({method: "put", body: JSON.stringify(simulationParameters)}));
        let typed_observable = observable.map(res => (res.json() as User));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /simulationParameters/delete/{id} delete
    public deleteSimulationParameters(id: number): Observable<ResultMessage | HttpErrorResponse> {
        let url = this.url_prefix;
        url += encodeURIComponent("simulationParameters/delete/");
        url += encodeURIComponent(id.toString());
        let observable = this.http.delete(url);
        let typed_observable = observable.map(res => (res.json() as ResultMessage));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /tasks/add post
    public addTask(task: Task): Observable<Task | HttpErrorResponse> {
        const url = this.url_prefix + "tasks/add";

        let observable = this.http.request(url, 
            new RequestOptions({method: "post", body: JSON.stringify(task)}));
        let typed_observable = observable.map(res => (res.json() as Task));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /tasks/get/{id} get
    public getTask(id: number): Observable<Task | HttpErrorResponse> {
        let url = this.url_prefix;
        url += encodeURIComponent("tasks/get/");
        url += encodeURIComponent(id.toString());
        let observable = this.http.get(url);
        let typed_observable = observable.map(res => (res.json() as Task));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /tasks/getAll get
    public getAllTasks(): Observable<Array<Task> | HttpErrorResponse> {
        const url = this.url_prefix + "tasks/getAll";

        let observable = this.http.get(url);
        let typed_observable = observable.map(res => (res.json() as Array<Task>));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /tasks/update put
    public updateTask(task: Task): Observable<Task | HttpErrorResponse> {
        const url = this.url_prefix + "tasks/update";

        let observable = this.http.request(url, 
            new RequestOptions({method: "put", body: JSON.stringify(task)}));
        let typed_observable = observable.map(res => (res.json() as Task));
        if (this.on_error) {
            return typed_observable.catch(err => this.on_error(err))
        }
        return typed_observable;
    }

    // Sends a request to the endpoint: /tasks/delete/{id} delete
    public deleteTask(id: number): Observable<any | HttpErrorResponse> {
        let url = this.url_prefix;
        url += encodeURIComponent("tasks/delete/");
        url += encodeURIComponent(id.toString());
        let observable = this.http.delete(url);
        if (this.on_error) {
            return observable.catch(err => this.on_error(err))
        }
        return observable;
    }
}

// Automatically generated file by swagger_to. DO NOT EDIT OR APPEND ANYTHING!

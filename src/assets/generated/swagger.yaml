---
openapi: 3.0.0
info:
  description: CSTEP API documentation
  version: v1
  title: CSTEP API
tags:
  - name: ts-client
servers:
- url: "/"
paths:
  /users/add:
    post:
      operationId: createUser
      tags:
      - User Controller
      summary: Creates a new user
      parameters:
       - name: user
         in: body
         required: true
         schema:
          $ref: '#/definitions/User'
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/User"
  /users/get/{id}:
    get:
      operationId: getUser
      tags:
      - User Controller
      summary: Reads a user with specified id
      parameters:
      - name: id
        in: path
        required: true
        type: number
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/User"
  /users/getAll:
    get:
      operationId: getAllUsers
      tags:
      - User Controller
      summary: Reads all users without protected fields
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            type: array
            items:
              $ref: "#/definitions/User"
  /users/update:
    put:
      operationId: updateUser
      tags:
      - User Controller
      summary: Updates an existing user
      parameters:
       - name: user
         in: body
         required: true
         schema:
          $ref: '#/definitions/User'
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/User"
  /users/delete/{id}:
    delete:
      operationId: deleteUser
      tags:
      - User Controller
      summary: Removes an existing user
      parameters:
      - name: id
        in: path
        required: true
        type: number
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/ResultMessage"
  /users/register:
    post:
      operationId: registerUser
      tags:
      - User Controller
      summary: Registers a new user
      parameters:
       - name: user
         in: body
         required: true
         schema:
          $ref: '#/definitions/User'
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/User"
  /users/login:
    post:
      operationId: login
      tags:
      - User Controller
      summary: Authenticates and authorizes user
      parameters:
       - name: loginParams
         in: body
         required: true
         schema:
          $ref: '#/definitions/LoginParams'
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/User"
  /simulationParameters/add:
    post:
      operationId: createSimulationParameters
      tags:
      - Simulation Parameters Controller
      summary: Creates a new simulation parameters
      parameters:
       - name: simulationParameters
         in: body
         required: true
         schema:
          $ref: "#/definitions/SimulationParameters"
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/SimulationParameters"
  /simulationParameters/get/{id}:
    get:
      operationId: getSimulationParameters
      tags:
      - Simulation Parameters Controller
      summary: Reads a simulation parameters with specified id
      parameters:
      - name: id
        in: path
        required: true
        type: number
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/SimulationParameters"
  /simulationParameters/getAll:
    get:
      operationId: getAllSimulationParameters
      tags:
      - Simulation Parameters Controller
      summary: Reads all siulation parameters without protected fields
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/SimulationParameters"
  /simulationParameters/update:
    put:
      operationId: updateSimulationParameters
      tags:
      - Simulation Parameters Controller
      summary: Updates an existing simulation parameters
      parameters:
       - name: simulationParameters
         in: body
         required: true
         schema:
          $ref: "#/definitions/SimulationParameters"
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/User"
  /simulationParameters/delete/{id}:
    delete:
      operationId: deleteSimulationParameters
      tags:
      - Simulation Parameters Controller
      summary: Removes an existing simulation parameters
      parameters:
      - name: id
        in: path
        required: true
        type: number
      produces:
        - application/json
      consumes:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/ResultMessage"
  /tasks/add:
    post:
      operationId: addTask
      tags:
      - Tasks Controller
      summary: Creates a new task
      parameters:
       - name: task
         in: body
         required: true
         schema:
          $ref: "#/definitions/Task"
      produces:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/Task"
  /tasks/get/{id}:
    get:
      operationId: getTask
      tags:
      - Tasks Controller
      summary: Reads a task with specified id
      parameters:
      - name: id
        in: path
        required: true
        type: number
      produces:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/Task"
  /tasks/getAll:
    get:
      operationId: getAllTasks
      tags:
      - Tasks Controller
      summary: Reads all tasks without protected fields
      produces:
        - application/json
      responses:
        '200':
          schema:
            type: array
            items:
              $ref: "#/definitions/Task"
  /tasks/update:
    put:
      operationId: updateTask
      tags:
      - Tasks Controller
      summary: Updates an existing task
      parameters:
       - name: task
         in: body
         required: true
         schema:
          $ref: "#/definitions/Task"
      produces:
        - application/json
      responses:
        '200':
          schema:
            $ref: "#/definitions/Task"
  /tasks/delete/{id}:
    delete:
      operationId: deleteTask
      tags:
      - Tasks Controller
      summary: Removes an existing task
      parameters:
      - name: id
        in: path
        required: true
        type: number
      produces:
        - application/json
      responses:
        '200':
          chema:
            $ref: "#/definitions/ResultMessage"
definitions:
  User:
    type: object
    required: 
      - login
      - password
      - email
      - first_name
      - last_name
      - index
      - user_role
    properties:
      id:
        type: number
        format: double
      login:
        type: string
      password:
        type: string
      email:
        type: string
        format: email
      first_name:
        type: string
      last_name:
        type: string
      index:
        type: string
      token:
        type: string
      user_role:
        type: number
        format: double
      group_id:
        type: string
  SimulationParameters:
    type: object
    required:
      - simulation_parameters_id
      - task_id
      - user_id
      - gain_k
      - gain_p
      - gain_i
    properties:
      id:
        type: number
        format: double
      simulation_parameters_id:
        type: string
      task_id:
        type: number
        format: double
      user_id:
        type: number
        format: double
      gain_k:
        type: number
        format: double
      gain_p:
        type: number
        format: double
      gain_i:
        type: number
        format: double
  Task:
    type: object
    required:
      - group_id
      - start_date
      - end_date
    properties:
      id:
        type: number
        format: double
      group_id:
        type: string
      start_date:
        type: string
        format: " yyyy-mm-dd hh:mm:ss"
      end_date:
        type: string
        format: " yyyy-mm-dd hh:mm:ss"
  LoginParams:
    type: object
    required:
      - login
      - password
    properties:
      login:
        type: string
      password:
        type: string
  ResultMessage:
    type: object
    properties:
      message:
        type: string
      code:
        type: number
        format: double

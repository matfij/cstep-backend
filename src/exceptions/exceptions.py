class BadRequest(Exception):
    status_code = 400


class Unauthorized(Exception):
    status_code = 401


class Forbidden(Exception):
    status_code = 403


class LackOfTokenHeader(Unauthorized):
    pass


class WrongTokenHeader(Unauthorized):
    pass


class NotFound(Exception):
    status_code = 404
    

class MethodNotAllowed(Exception):
    status_code = 405

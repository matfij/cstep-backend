from flask import Blueprint, jsonify, views, current_app
from .exceptions import BadRequest, Unauthorized, Forbidden, NotFound, MethodNotAllowed
from src.utils.extensions import i18n


handler = Blueprint('errors', __name__)

@handler.app_errorhandler(Exception)
def handle_exception_format(error):
    payload = {}
    payload['message'] = i18n.t('main.exceptions.httpInernalServerError') + ' - ' + str(error)
    return jsonify(payload), 500

@handler.app_errorhandler(BadRequest)
def handle_bad_request_format(error):
    payload = {}
    payload['message'] = error.args
    payload['type'] = error.__class__.__name__
    return jsonify(payload), error.status_code

@handler.app_errorhandler(Unauthorized)
def handle_unauthorized_request_format(error):
    payload = {}
    payload['message'] = i18n.t('main.exceptions.httpUnauthorized')
    payload['type'] = error.__class__.__name__
    return jsonify(payload), error.status_code

@handler.app_errorhandler(Forbidden)
def handle_forbidden_request_format(error):
    payload = {}
    payload['message'] = i18n.t('main.exceptions.httpForbidden')
    payload['type'] = error.__class__.__name__
    return jsonify(payload), error.status_code

@handler.app_errorhandler(NotFound)
def handle_forbidden_request_format(error):
    payload = {}
    payload['message'] = i18n.t('main.exceptions.httpNotFound')
    payload['type'] = error.__class__.__name__
    return jsonify(payload), error.status_code

@handler.app_errorhandler(MethodNotAllowed)
def handle_forbidden_request_format(error):
    payload = {}
    payload['message'] = i18n.t('main.exceptions.httpMethodNotAllowed')
    payload['type'] = error.__class__.__name__
    return jsonify(payload), error.status_code

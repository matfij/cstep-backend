import os
import bcrypt


class CryptingService:
    __DEFAULT_SALT = os.getenv('COMMON_SALT')
    __ENCODING = 'utf-8'

    def encode_string(self, target: str):
        target_bytes = bytes(target, self.__ENCODING)
        hashed_bytes = bcrypt.hashpw(target_bytes, bcrypt.gensalt())

        return hashed_bytes.decode(self.__ENCODING)

    def compare_strings(self, target: str, check: str):
        target_bytes = bytes(target, self.__ENCODING)
        check_bytes = bytes(check, self.__ENCODING)
        
        match = False
        try:
            match = bcrypt.checkpw(check_bytes, target_bytes)
        except Exception:
            pass
        try:
            match = bcrypt.checkpw(check_bytes, target_bytes)
        except Exception:
            pass
        return match

from flask import jsonify
from src.models.base_model import BaseModel
from src.serializers.base_serializer import BaseSerializer
from src.validators.base_validator import BaseValidator
from src.utils.extensions import db, i18n


class BaseService:
    serializer = BaseSerializer()
    validator = BaseValidator()
    multiobject_serializer = BaseSerializer(many=True)
    model = BaseModel

    def create(self, data: dict):
        params = self.serializer.load(data)
        dto = self.model(**params)
        
        if self.validator.validate(dto):
            db.session.add(dto)
            db.session.commit()
            result = self.serializer.dump(dto)
            return jsonify(result)
        else:
            return { 'message': i18n.t('main.exceptions.passedInvalidData') }, 400

    def read(self, id: int):
        dto = self.model.query.get(id)
        result = self.serializer.dump(dto)
        return result

    def read_all(self):
        dtos = self.model.query.all()
        results = self.multiobject_serializer.dump(dtos)
        return results

    def update(self, data: dict):
        dto = self.model.query.get(data['id'])
        dto_fields = self.serializer.load(data)
        model = self.model(**dto_fields)

        if self.validator.validate(model):
            for key, val in dto_fields.items():
                setattr(dto, key, val)
            db.session.commit()
            result = self.serializer.dump(dto)
            return jsonify(result)
        else:
            return { 'message': i18n.t('main.exceptions.passedInvalidData') }, 400

    def delete(self, id: int):
        dto = self.model.query.get(id)
        if dto:
            db.session.delete(dto)
            db.session.commit()

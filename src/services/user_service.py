import json
from .base_service import BaseService
from src.models.user_model import User, UserRole, UpdatePasswordParams
from src.serializers.user_serializer import UserSerializer
from src.utils.extensions import db, i18n
from src.services.crypting_service import CryptingService
from src.validators.user_validator import UserValidator


class UserService(BaseService):
    serializer = UserSerializer()
    validator = UserValidator()
    multiobject_serializer = UserSerializer(many=True)
    model = User
    crypting_service = CryptingService()

    def register(self, data: dict):
        params = self.serializer.load(data)
        dto = self.model(**params)
        
        if self.validator.validate(dto):
            dto.user_role = UserRole.student.value
            dto.password = self.crypting_service.encode_string(dto.password)
            try:
                db.session.add(dto)
                db.session.commit()
                result = self.serializer.dump(dto)
                return result
            except Exception as ex:
                return { 'message': str(ex) }, 400
        else:
            return { 'message': i18n.t('main.exceptions.passedInvalidData') }, 400

    def update_password(self, data: dict):
        dto = self.model.query.get(data['id'])
        update_params = UpdatePasswordParams(data)
        
        if self.crypting_service.compare_strings(getattr(dto, 'password'), update_params.old_password):
            if len(update_params.new_password) < 5:
                return { 'message': i18n.t('main.exceptions.passwordTooShort') }, 400
            else:
                new_password = self.crypting_service.encode_string(update_params.new_password)
                setattr(dto, 'password', new_password)
                db.session.commit()
                result = self.serializer.dump(dto)
                return result
        else:
            return { 'message': i18n.t('main.exceptions.oldPasswordDontMatch') }, 400

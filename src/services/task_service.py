from .base_service import BaseService
from src.models.task_model import Task
from src.serializers.task_serializer import TaskSerializer
from src.validators.task_validator import TaskValidator


class TaskService(BaseService):
    serializer = TaskSerializer()
    validator = TaskValidator()
    multiobject_serializer = TaskSerializer(many=True)
    model = Task

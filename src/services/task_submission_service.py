from .base_service import BaseService
from src.models.task_submission_model import TaskSubmission
from src.serializers.task_submission_serializer import TaskSubmissionSerializer
from src.validators.task_submission_validator import TaskSubmissionValidator


class TaskSubmissionService(BaseService):
    serializer = TaskSubmissionSerializer()
    validator = TaskSubmissionValidator()
    multiobject_serializer = TaskSubmissionSerializer(many=True)
    model = TaskSubmission

    def read_all(self):
        dtos = self.model.query.with_entities(
            self.model.id, 
            self.model.task_id,
            self.model.task_title,
            self.model.creator_name,
            self.model.creator_id,
            self.model.rate,
            self.model.comment,
            self.model.submission_date,
            self.model.rate_date
        )
        results = self.multiobject_serializer.dump(dtos)
        return results

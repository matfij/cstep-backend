import jwt
import requests
import datetime
from flask import current_app, request
from src.models.user_model import User, UserRole
from src.serializers.user_serializer import UserSerializer
from src.utils.extensions import db
from src.utils.config import Config
from src.exceptions.exceptions import Unauthorized, Forbidden, LackOfTokenHeader, WrongTokenHeader
from src.utils.extensions import i18n
from src.services.crypting_service import CryptingService


class AuthService:
    serializer = UserSerializer()
    multiobject_serializer = UserSerializer(many=True)
    model = User
    crypting_service = CryptingService()

    def login(self, data: dict):
        if 'login' in data and 'password' in data:
            dto = self.model.query.filter_by(login=data['login']).first()
            if dto:
                result = self.serializer.dump(dto)
                
                if self.crypting_service.compare_strings(result['password'], data['password']):
                    token = jwt.encode({
                        'user': result,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=360000)
                    },
                    current_app.config['PUBLIC_AUTH_KEY']
                    )
                    result['token'] = token
                    return result
                else:
                    return { 'message': i18n.t('main.exceptions.loginPassword') }, 400
            else:
                return { 'message': i18n.t('main.exceptions.userNotFound') }, 400
        else:
            return { 'message': i18n.t('main.exceptions.emptyRequest') }, 400

    def is_auth(self):
        token = request.headers.get('Authorization', None)
        if not token:
            raise LackOfTokenHeader
        try:
            key = current_app.config['PUBLIC_AUTH_KEY']
            decoded_token = jwt.decode(token, current_app.config['PUBLIC_AUTH_KEY'], algorithms="HS256")
        except jwt.ExpiredSignatureError:
            raise Unauthorized
        except Exception:
            raise WrongTokenHeader
        return decoded_token['user']

    def is_admin(self):
        token = request.headers.get('Authorization', None)
        key = current_app.config['PUBLIC_AUTH_KEY']
        decoded_token = jwt.decode(token, current_app.config['PUBLIC_AUTH_KEY'], algorithms="HS256")
        if decoded_token['user']['user_role'] == UserRole.admin.value:
            return decoded_token['user']
        else:
            raise Forbidden

    def refresh_token(self, data: dict):
        serializer = UserSerializer()
        user = serializer.load(data)
        dto = serializer.dump(user)
        token = jwt.encode({
            'user': dto,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600)
        },
        current_app.config['PUBLIC_AUTH_KEY']
        )
        return { 'token': token }

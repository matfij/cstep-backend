from sqlalchemy.sql import func
from src.utils.extensions import db
from .base_model import BaseModel


class TaskSubmission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task_id = db.Column(db.Integer)
    task_title = db.Column(db.String(100))
    report_file = db.Column(db.String())
    creator_name = db.Column(db.String(100))
    creator_id = db.Column(db.Integer)
    rate = db.Column(db.String(100))
    comment = db.Column(db.String(1000))
    submission_date = db.Column(db.String(100))
    rate_date = db.Column(db.String(100))

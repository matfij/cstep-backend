from sqlalchemy.sql import func
from src.utils.extensions import db


class BaseModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)

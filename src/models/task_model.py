from sqlalchemy.sql import func
from src.utils.extensions import db
from .base_model import BaseModel


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    group_id = db.Column(db.String(100))
    creator_name = db.Column(db.String(100))
    creator_id = db.Column(db.Integer)
    description = db.Column(db.String(1000))
    title = db.Column(db.String(100))
    start_date = db.Column(db.String(100))
    end_date = db.Column(db.String(100))

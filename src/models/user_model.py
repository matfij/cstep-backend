import enum
from sqlalchemy.sql import func
from src.utils.extensions import db
from .base_model import BaseModel


class UserRole(enum.Enum):
    admin = 0
    tutor = 1
    student = 2


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    email = db.Column(db.String(100), unique=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    index = db.Column(db.String(100), unique=True)
    token = db.Column(db.String(1000))
    user_role = db.Column(db.Integer)
    group_id = db.Column(db.String(100))


class UpdatePasswordParams:
    id: int
    old_password: str
    new_password: str

    def __init__(self, data: dict):
        self.id = data['id']
        self.old_password = data['old_password']
        self.new_password = data['new_password']

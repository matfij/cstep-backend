from .base_serializer import BaseSerializer
from src.models.task_model import Task


class TaskSerializer(BaseSerializer):
    class Meta:
        model = Task
        fields = ('id', 'creator_name', 'creator_id', 'title', 'description', 'group_id', 'start_date', 'end_date')

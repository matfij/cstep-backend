from .base_serializer import BaseSerializer
from src.models.user_model import User


class UserSerializer(BaseSerializer):
    class Meta:
        model = User
        fields = ('id', 'login', 'password', 'email', 'first_name', 'last_name', 'index', 'token', 'user_role', 'group_id')

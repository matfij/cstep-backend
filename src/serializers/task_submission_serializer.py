from .base_serializer import BaseSerializer
from src.models.task_submission_model import TaskSubmission


class TaskSubmissionSerializer(BaseSerializer):
    class Meta:
        model = TaskSubmission
        fields = ('id', 'task_title', 'task_id', 'report_file', 'creator_name', 'creator_id', 'rate', 'comment', 'submission_date', 'rate_date')

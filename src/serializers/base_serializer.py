from src.utils.extensions import ma
from src.models.base_model import BaseModel


class BaseSerializer(ma.SQLAlchemyAutoSchema):
    pass
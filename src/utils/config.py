import os


class Config(object):
    DEBUG = False
    TESTING = False
    ALLOWED_EXTENSIONS = ['pdf']
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    PUBLIC_AUTH_KEY = os.getenv('PUBLIC_AUTH_KEY')


class ProductionConfig(Config):
    SECRET_KEY = os.getenv('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_POOL_RECYCLE = 90
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True
    SECRET_KEY = os.getenv('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_POOL_RECYCLE = 90
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    TESTING = True
    PUBLIC_AUTH_KEY = 'AR6481703MEHM0JAX'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///tests/test-sqlite.db'
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_cors import CORS
import i18n


db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
cors = CORS(supports_credentials=True)
i18n.set('locale', 'en')
i18n.set('fallback', 'en')
i18n.load_path.append('src/assets/i18n')

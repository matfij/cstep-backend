from functools import wraps
from src.services.auth_service import AuthService


def is_auth(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        service = AuthService()
        service.is_auth()
        return func(*args, **kwargs)
    return wrapper

def is_admin(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        service = AuthService()
        return func(*args, **kwargs)
    return wrapper

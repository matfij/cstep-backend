from flask import request
from flask_api import FlaskAPI
from flask_cors import CORS
import RPi.GPIO as GPIO


app = FlaskAPI(__name__)
cors = CORS(supports_credentials=True)
cors.init_app(app)

GPIO.setmode(GPIO.BOARD)

LED_THRESHOLD = 20

LEDs = {
    'gain_k': 16,
    'gain_p': 18,
    'gain_i': 20
}
for key, port in LEDs:
    GPIO.setup(port, GPIO.OUT)


@app.route('/', methods=['GET'])
def server_test():
    return { 'status': 'Connected successfully' }
  
@app.route('/setParameters', methods=['POST'])
def set_parameters():
    try:
        data = request.get_json()

        for key, port in LEDs:
            GPIO.output(port, 0)
            if data[key] > LED_THRESHOLD:
                GPIO.output(port, 1)

        return { 'status': 'Parameters updated' }

    except Exception as ex:
        return { 'status': 'Internal server error: ${ex}' }


if __name__ == '__main__':
    app.run(debug=True)

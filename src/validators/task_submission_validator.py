import re
from src.validators.base_validator import BaseValidator
from src.models.task_submission_model import TaskSubmission


class TaskSubmissionValidator(BaseValidator):
    __MIN_DESCRIPTION_LENGTH = 20
    __DATE_PATTERN = re.compile("([0-9]{2})-([0-9]{2})-([0-9]{4})")

    def validate(self, model: TaskSubmission) -> bool:
        if model.task_id is None:
            return False
        elif model.creator_id is None or model.creator_name is None:
            return False
        elif not self.__DATE_PATTERN.match(model.submission_date):
            return False
        else:
            return True

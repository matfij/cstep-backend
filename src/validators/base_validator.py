from src.models.base_model import BaseModel


class BaseValidator:

    def validate(self, model: BaseModel) -> bool:
        return True

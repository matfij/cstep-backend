import re
from src.validators.base_validator import BaseValidator
from src.models.user_model import User


class UserValidator(BaseValidator):
    __MIN_LOGIN_LENGTH = 3
    __MIN_PASSWORD_LENGTH = 5
    __EMAIL_PATTERN = re.compile("[^@]+@[^@]+\\.[^@]+")
    __INDEX_LENGTH = 6

    def validate(self, model: User) -> bool:
        if len(model.login) < self.__MIN_LOGIN_LENGTH:
            return False
        elif len(model.password) < self.__MIN_PASSWORD_LENGTH:
            return False
        elif not self.__EMAIL_PATTERN.match(model.email):
            return False
        elif model.first_name is None or model.last_name is None:
            return False
        elif len(model.index) != self.__INDEX_LENGTH:
            return False
        else:
            return True

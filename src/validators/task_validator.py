import re
from src.validators.base_validator import BaseValidator
from src.models.task_model import Task


class TaskValidator(BaseValidator):
    __MIN_DESCRIPTION_LENGTH = 20
    __DATE_PATTERN = re.compile("([0-9]{4})-([0-9]{2})-([0-9]{2})")

    def validate(self, model: Task) -> bool:
        if model.creator_id is None or model.creator_name is None:
            return False
        elif model.title is None or len(model.description) < self.__MIN_DESCRIPTION_LENGTH:
            return False
        elif not self.__DATE_PATTERN.match(model.start_date) or not self.__DATE_PATTERN.match(model.end_date):
            return False
        else:
            return True

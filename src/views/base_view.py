import os
from flask import request, flash, redirect, url_for, send_file, Blueprint, views, jsonify, current_app
from sqlalchemy.exc import OperationalError, IntegrityError
from src.services.base_service import BaseService
from src.utils.decorators import is_auth, is_admin


class BaseView(views.MethodView):
    service = BaseService()

    @is_auth
    def post(self):
        data = request.get_json()
        return self.service.create(data)

    @is_auth
    def get(self, id: int = None):
        if id:
            result = self.service.read(id)
            return jsonify(result)
        else:
            results = self.service.read_all()
            return jsonify(results)

    @is_auth
    def put(self):
        data = request.get_json()
        return self.service.update(data)

    @is_auth
    @is_admin
    def delete(self, id: int):
        self.service.delete(id)
        return jsonify({})

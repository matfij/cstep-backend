from flask import request, Blueprint
from .base_view import BaseView
from src.services.user_service import UserService
from src.services.auth_service import AuthService
from src.utils.decorators import is_auth, is_admin
from src.serializers.user_serializer import UserSerializer
from src.models.user_model import User


blueprint = Blueprint('user', __name__)


class UserView(BaseView):
    service = UserService()
    

def register():
    service = UserService()
    data = request.get_json()
    result = service.register(data)
    return result

def login():
    service = AuthService()
    data = request.get_json()
    result = service.login(data)
    return result

def refresh_token():
    service = AuthService()
    data = request.get_json()
    result = service.refresh_token(data)
    return result

def update_password():
    service = UserService()
    data = request.get_json()
    result = service.update_password(data)
    return result


view = UserView.as_view('user_view')
blueprint.add_url_rule('/add', view_func=view, methods=['POST'])
blueprint.add_url_rule('/get/<int:id>', view_func=view, methods=['GET'])
blueprint.add_url_rule('/getAll', view_func=view, methods=['GET'])
blueprint.add_url_rule('/delete/<int:id>', view_func=view, methods=['DELETE'])

blueprint.add_url_rule('/login', view_func=login, methods=['POST'])
blueprint.add_url_rule('/register', view_func=register, methods=['POST'])
blueprint.add_url_rule('/refresh', view_func=refresh_token, methods=['POST'])
blueprint.add_url_rule('/updatePassword', view_func=update_password, methods=['PUT'])

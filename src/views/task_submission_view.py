from flask import Blueprint
from .base_view import BaseView
from src.services.task_submission_service import TaskSubmissionService


blueprint = Blueprint('task_submission', __name__)


class TaskSubmissionView(BaseView):
    service = TaskSubmissionService()


view = TaskSubmissionView.as_view('task_submission_view')
blueprint.add_url_rule('/add', view_func=view, methods=['POST'])
blueprint.add_url_rule('/get/<int:id>', view_func=view, methods=['GET'])
blueprint.add_url_rule('/getAll', view_func=view, methods=['GET'])
blueprint.add_url_rule('/update', view_func=view, methods=['PUT'])
blueprint.add_url_rule('/delete/<int:id>', view_func=view, methods=['DELETE'])

from flask import Blueprint
from .base_view import BaseView, is_auth
from src.services.task_service import TaskService
from flask import request, flash, redirect, url_for, send_file, Blueprint, views, jsonify, current_app

blueprint = Blueprint('task', __name__)


class TaskView(BaseView):
    service = TaskService()


view = TaskView.as_view('task_view')
blueprint.add_url_rule('/add', view_func=view, methods=['POST'])
blueprint.add_url_rule('/get/<int:id>', view_func=view, methods=['GET'])
blueprint.add_url_rule('/getAll', view_func=view, methods=['GET'])
blueprint.add_url_rule('/update', view_func=view, methods=['PUT'])
blueprint.add_url_rule('/delete/<int:id>', view_func=view, methods=['DELETE'])

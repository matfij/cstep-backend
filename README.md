# Control System Theory - Educational Platform Backend
Backend part of a C.S.T.E.P. project realized for the Mechatronic Design course at the AGH University.
Build with Python and Flask.

## Requirements
```
pip install -r requirements.txt
pip freeze > requirements.txt
```

## Virtual environment 
```
py -m venv .venv
.venv\Scripts\activate
deactivate
```

## Starting backend - development
```
py run.py
```

## Migration
```
py db_manager.py db init
py db_manager.py db migrate
py db_manager.py db upgrade
```

## Client generation
```
py client_manager.py --swagger_path src/assets/generated/swagger.yaml --outpath src/assets/generated/client.ts
```

## Running Raspberri Pi server
```
export FLASK_APP=fl-app.py
sudo -E flask run --host=0.0.0.0 --port=4444
```

## Running tests
```
py test_db_manager.py db init --directory=tests/migrations
py test_db_manager.py db migrate --directory=tests/migrations
py test_db_manager.py db upgrade --directory=tests/migrations

pytest test_manager.py
```

## Common Heroku commands
```
heroku run bash
git push heroku master
heroku logs --tail
```

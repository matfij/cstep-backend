from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from src.utils.extensions import db, ma, migrate, cors
from src.utils.config import Config, TestingConfig
from src.models.user_model import User
from src.models.task_model import Task
from src.models.task_submission_model import TaskSubmission


app = Flask(__name__)
app.config.from_object(TestingConfig)

db.init_app(app)
migrate.init_app(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

manager.run()
db.create_all()

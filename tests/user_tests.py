import json
from src.utils.extensions import i18n


def test_user_register_correct(client):
    rv = client.delete('/api/v1/users/delete/1')

    user_data = { 
        'login': 'login', 
        'password': 'password', 
        'email': 'email@example.com',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'index': '000000'
    }
    rv = client.post('/api/v1/users/register', data=json.dumps(user_data), content_type='application/json')
    rv_user = json.loads(rv.data.decode('utf-8'))

    user_data['user_role'] = 2

    assert user_data['login'] == rv_user['login']
    assert user_data['email'] == rv_user['email']
    assert user_data['first_name'] == rv_user['first_name']
    assert user_data['last_name'] == rv_user['last_name']
    assert user_data['index'] == rv_user['index']
    assert user_data['user_role'] == rv_user['user_role']


def test_user_register_with_incorrect_email(client):
    user_data = { 
        'login': 'login1', 
        'password': 'password', 
        'email': 'email.example.com',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'index': '000001'
    }
    rv = client.post('/api/v1/users/register', data=json.dumps(user_data), content_type='application/json')
    rv_data = json.loads(rv.data.decode('utf-8'))
    
    assert i18n.t('main.exceptions.passedInvalidData') == rv_data['message']

def test_user_login_success(client):
    user_data = { 
        'login': 'login', 
        'password': 'password'
    }
    rv = client.post('/api/v1/users/login', data=json.dumps(user_data), content_type='application/json')
    rv_user = json.loads(rv.data.decode('utf-8'))

    user_data = { 
        'email': 'email@example.com',
        'first_name': 'first_name',
        'login': 'login', 
        'last_name': 'last_name',
        'index': '000000',
        'user_role': 2
    }
    print(rv_user)
    assert user_data['login'] == rv_user['login']
    assert user_data['email'] == rv_user['email']
    assert user_data['first_name'] == rv_user['first_name']
    assert user_data['last_name'] == rv_user['last_name']
    assert user_data['index'] == rv_user['index']
    assert user_data['user_role'] == rv_user['user_role']


def test_user_login_fail(client):
    user_data = { 
        'login': 'login', 
        'password': 'password1'
    }
    rv = client.post('/api/v1/users/login', data=json.dumps(user_data), content_type='application/json')
    rv_data = json.loads(rv.data.decode('utf-8'))

    assert i18n.t('main.exceptions.loginPassword') == rv_data['message']


def test_user_update_password_success(client):
    user_data = { 
        'login': 'login', 
        'password': 'password'
    }
    rv = client.post('/api/v1/users/login', data=json.dumps(user_data), content_type='application/json')
    rv_user = json.loads(rv.data.decode('utf-8'))

    user_data = { 
        'id': rv_user['id'],
        'old_password': 'password',
        'new_password': 'goodpass123'
    }
    rv = client.put('/api/v1/users/updatePassword', 
        data=json.dumps(user_data), 
        content_type='application/json',  
        headers={ 'Authorization': rv_user['token'] }
    )
    rv_data = json.loads(rv.data.decode('utf-8'))
    
    assert rv_user['id'] == rv_data['id']
    assert 'login' == rv_data['login']


def test_user_update_password_fail(client):
    user_data = { 
        'login': 'login', 
        'password': 'goodpass123'
    }
    rv = client.post('/api/v1/users/login', data=json.dumps(user_data), content_type='application/json')
    rv_user = json.loads(rv.data.decode('utf-8'))

    user_data = { 
        'id': rv_user['id'],
        'old_password': 'password1',
        'new_password': 'goodpass123'
    }
    rv = client.put('/api/v1/users/updatePassword', 
        data=json.dumps(user_data), 
        content_type='application/json',  
        headers={ 'Authorization': rv_user['token'] }
    )
    rv_data = json.loads(rv.data.decode('utf-8'))
    
    assert i18n.t('main.exceptions.oldPasswordDontMatch') == rv_data['message']

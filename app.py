import os
from flask import Flask
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from src.utils.config import Config, DevelopmentConfig, ProductionConfig
from src.utils.extensions import db, ma, migrate, cors
from src.views import user_view, task_view, task_submission_view
from src.exceptions import exceptions_view


def create_app(config):
   app = Flask(__name__)
   app.config.from_object(config)

   app.register_blueprint(exceptions_view.handler)
   app.register_blueprint(user_view.blueprint, url_prefix='/api/v1/users')
   app.register_blueprint(task_view.blueprint, url_prefix='/api/v1/tasks')
   app.register_blueprint(task_submission_view.blueprint, url_prefix='/api/v1/taskSubmissions')
   swagger_blueprint = get_swaggerui_blueprint('/swagger', '/static/swagger.json')
   app.register_blueprint(swagger_blueprint)

   db.init_app(app)
   ma.init_app(app)
   migrate.init_app(app, db)
   cors.init_app(app)

   return app


if __name__ == '__main__':
   app = create_app(DevelopmentConfig)
   app.run(Config.DEBUG)
else:
   app = create_app(ProductionConfig)

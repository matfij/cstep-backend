import os
import json
import pytest
import app as setup
from tests.user_tests import *
from src.utils.config import TestingConfig


@pytest.fixture
def client():
    app = setup.create_app(TestingConfig)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///tests/test-sqlite.db'
    
    context = app.app_context()
    context.push()

    with context:
        with app.test_client() as client:
            yield client

    context.pop()


def test_server_started(client):
    rv = client.post('/')

    assert b'' in rv.data
